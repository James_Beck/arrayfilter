const Words = ["spray", "limit", "elite", "exuberant", "destruction", "present", "happy"];
let LongWords = Words.filter(word => word.length > 6);

document.write(Words + "<br>");
document.write(LongWords + "<br><br>");

function IsBigEnough(value) {
    return value >= 10;
}

var Filtered = [12, 5, 8, 130, 44].filter(IsBigEnough);
document.write(Filtered + "<br><br>");

